document.addEventListener('DOMContentLoaded', (event) => {

    /**
     * Обработка состояний карточек (default, default_hover, selected, selected_hover)
     */
    let products = document.querySelectorAll(".product-blk");
    let firstProduct = products.item(0);
    let secondProduct = products.item(1);
    let thirdProduct = products.item(2);

    for (let product of products) {

        if (!product.closest('.product').classList.contains('disabled')) {
            product.addEventListener('mouseenter', (event) => {

                let targetElement = event.target || event.srcElement;

                let productTriangle = targetElement.getElementsByClassName('product-triangle')[0];
                let productTop = targetElement.getElementsByClassName('product-top')[0];
                let productBottom = targetElement.getElementsByClassName('product-bottom')[0];
                let productCircle = targetElement.getElementsByClassName('product-weight')[0];
                let productHeader = targetElement.getElementsByClassName('product-header')[0];

                if (productCircle.classList.contains('selected-circle')) {
                    productTriangle.classList.remove('selected-border-angle');
                    productTriangle.classList.add('selected-border-angle-hover');

                    productTop.classList.remove('selected-border-top');
                    productTop.classList.remove('selected-border-right');
                    productTop.classList.add('selected-border-top-hover');
                    productTop.classList.add('selected-border-right-hover');

                    productBottom.classList.remove('selected-border-left');
                    productBottom.classList.remove('selected-border-right');
                    productBottom.classList.remove('selected-border-bottom');
                    productBottom.classList.add('selected-border-left-hover');
                    productBottom.classList.add('selected-border-right-hover');
                    productBottom.classList.add('selected-border-bottom-hover');

                    productCircle.classList.remove('selected-circle');
                    productCircle.classList.add('selected-circle-hover');

                    productHeader.innerHTML = '<span class="product-header-selected-hover">Котэ не одобряет?</span>';
                } else {
                    productTriangle.classList.remove('default-border-angle');
                    productTriangle.classList.add('default-border-angle-hover');

                    productTop.classList.remove('default-border-top');
                    productTop.classList.remove('default-border-right');
                    productTop.classList.add('default-border-top-hover');
                    productTop.classList.add('default-border-right-hover');

                    productBottom.classList.remove('default-border-left');
                    productBottom.classList.remove('default-border-right');
                    productBottom.classList.remove('default-border-bottom');
                    productBottom.classList.add('default-border-left-hover');
                    productBottom.classList.add('default-border-right-hover');
                    productBottom.classList.add('default-border-bottom-hover');

                    productCircle.classList.remove('default-circle');
                    productCircle.classList.add('default-circle-hover');
                }
            });

            product.addEventListener('mouseleave', (event) => {
                let targetElement = event.target || event.srcElement;

                let productTriangle = targetElement.getElementsByClassName('product-triangle')[0];
                let productTop = targetElement.getElementsByClassName('product-top')[0];
                let productBottom = targetElement.getElementsByClassName('product-bottom')[0];
                let productCircle = targetElement.getElementsByClassName('product-weight')[0];
                let productHeader = targetElement.getElementsByClassName('product-header')[0];

                if (productCircle.classList.contains('selected-circle-hover')) {
                    productTriangle.classList.add('selected-border-angle');
                    productTriangle.classList.remove('selected-border-angle-hover');

                    productTop.classList.add('selected-border-top');
                    productTop.classList.add('selected-border-right');
                    productTop.classList.remove('selected-border-top-hover');
                    productTop.classList.remove('selected-border-right-hover');

                    productBottom.classList.add('selected-border-left');
                    productBottom.classList.add('selected-border-right');
                    productBottom.classList.add('selected-border-bottom');
                    productBottom.classList.remove('selected-border-left-hover');
                    productBottom.classList.remove('selected-border-right-hover');
                    productBottom.classList.remove('selected-border-bottom-hover');

                    productCircle.classList.add('selected-circle');
                    productCircle.classList.remove('selected-circle-hover');

                    productHeader.innerHTML = 'Сказочное заморское яство';
                } else {
                    productTriangle.classList.add('default-border-angle');
                    productTriangle.classList.remove('default-border-angle-hover');

                    productTop.classList.add('default-border-top');
                    productTop.classList.add('default-border-right');
                    productTop.classList.remove('default-border-top-hover');
                    productTop.classList.remove('default-border-right-hover');

                    productBottom.classList.add('default-border-left');
                    productBottom.classList.add('default-border-right');
                    productBottom.classList.add('default-border-bottom');
                    productBottom.classList.remove('default-border-left-hover');
                    productBottom.classList.remove('default-border-right-hover');
                    productBottom.classList.remove('default-border-bottom-hover');

                    productCircle.classList.add('default-circle');
                    productCircle.classList.remove('default-circle-hover');
                }
            });

            product.addEventListener('click', (event) => {
                let targetElement = event.target || event.srcElement;

                targetElement = targetElement.closest('.product');

                let productTriangle = targetElement.getElementsByClassName('product-triangle')[0];
                let productTop = targetElement.getElementsByClassName('product-top')[0];
                let productBottom = targetElement.getElementsByClassName('product-bottom')[0];
                let productCircle = targetElement.getElementsByClassName('product-weight')[0];
                let productHeader = targetElement.getElementsByClassName('product-header')[0];

                if (productCircle.classList.contains('selected-circle-hover')) {
                    productTriangle.classList.remove('selected-border-angle-hover');
                    productTriangle.classList.add('default-border-angle-hover');

                    productTop.classList.remove('selected-border-top-hover');
                    productTop.classList.remove('selected-border-right-hover');
                    productTop.classList.add('default-border-top-hover');
                    productTop.classList.add('default-border-right-hover');

                    productBottom.classList.remove('selected-border-left-hover');
                    productBottom.classList.remove('selected-border-right-hover');
                    productBottom.classList.remove('selected-border-bottom-hover');
                    productBottom.classList.add('default-border-left-hover');
                    productBottom.classList.add('default-border-right-hover');
                    productBottom.classList.add('default-border-bottom-hover');

                    productCircle.classList.remove('selected-circle-hover');
                    productCircle.classList.add('default-circle-hover');

                    productHeader.innerHTML = 'Сказочное заморское яство';
                } else {
                    productTriangle.classList.remove('default-border-angle');
                    productTriangle.classList.remove('default-border-angle-hover');
                    productTriangle.classList.add('selected-border-angle-hover');

                    productTop.classList.add('selected-border-top-hover');
                    productTop.classList.add('selected-border-right-hover');
                    productTop.classList.remove('default-border-top-hover');
                    productTop.classList.remove('default-border-right-hover');
                    productTop.classList.remove('default-border-top');
                    productTop.classList.remove('default-border-right');

                    productBottom.classList.add('selected-border-left-hover');
                    productBottom.classList.add('selected-border-right-hover');
                    productBottom.classList.add('selected-border-bottom-hover');
                    productBottom.classList.remove('default-border-left-hover');
                    productBottom.classList.remove('default-border-right-hover');
                    productBottom.classList.remove('default-border-bottom-hover');
                    productBottom.classList.remove('default-border-left');
                    productBottom.classList.remove('default-border-right');
                    productBottom.classList.remove('default-border-bottom');

                    productCircle.classList.add('selected-circle-hover');
                    productCircle.classList.remove('default-circle-hover');
                    productCircle.classList.remove('default-circle');
                    productCircle.classList.remove('selected-circle');

                    productHeader.innerHTML = '<span class="product-header-selected-hover">Котэ не одобряет?</span>';
                }
            });
        }
    }

    /**
     * Редактирование видимости карточки
     *
     * Чтобы сделать карточку недоступной для покупки нужно добавить класс "disabled" в div.product требуемой карточки
     */

    let disabledProducts = document.querySelectorAll(".disabled");

    for (let disabledProduct of disabledProducts) {
        let productBlk = disabledProduct.getElementsByClassName('product-blk').item(0);

        productBlk.innerHTML += '<div class="disabled-triangle"></div>';
        productBlk.innerHTML += '<div class="disabled-top"></div>';
        productBlk.innerHTML += '<div class="disabled-bottom"></div>';
        productBlk.innerHTML += '<div class="disabled-sector"></div>';

        let productBottom = productBlk.getElementsByClassName('product-bottom').item(0);
        productBottom.style.borderColor = 'gray';

        let productTop = productBlk.getElementsByClassName('product-top').item(0);
        productTop.style.borderColor = 'gray';

        let productTriangle = productBlk.getElementsByClassName('product-triangle').item(0);
        productTriangle.style.borderColor = 'gray';

        let productCircle = productBlk.getElementsByClassName('product-weight').item(0);
        productCircle.style.backgroundColor = 'gray';

        let productText = disabledProduct.getElementsByClassName('product-text').item(0);

        if (productBlk.classList.contains('product-3')) {
            productText.innerHTML = 'Печалька, с курой закончился.';
            productText.classList.add('product-text-disabled');
        }

        if (productBlk.classList.contains('product-2')) {
            productText.innerHTML = 'Печалька, с рыбой закончился.';
            productText.classList.add('product-text-disabled');
        }

        if (productBlk.classList.contains('product-1')) {
            productText.innerHTML = 'Печалька, с фуа-гра закончился.';
            productText.classList.add('product-text-disabled');
        }
    }

    /**
     * Изменение текста под карточками в зависимости от состояния
     */

    firstProduct.addEventListener('click', (event) => {

        if (!firstProduct.closest('.product').classList.contains('disabled')) {
            let text = document.getElementsByClassName('product-1-text')[0];

            let targetElement = event.target || event.srcElement;

            targetElement = targetElement.closest('.product');

            let productCircle = targetElement.getElementsByClassName('product-weight')[0];

            if (productCircle.classList.contains('selected-circle-hover'))
                text.innerHTML = 'Печень утки разварная с артишоками.';
            else
                text.innerHTML = 'Чего сидишь? Порадуй котэ, <span class="buy buy-btn-1" onclick="buy(event)">купи</span>.';
        }
    });

    secondProduct.addEventListener('click', (event) => {

        if (!secondProduct.closest('.product').classList.contains('disabled')) {
            let text = document.getElementsByClassName('product-2-text')[0];

            let targetElement = event.target || event.srcElement;

            targetElement = targetElement.closest('.product');

            let productCircle = targetElement.getElementsByClassName('product-weight')[0];

            if (productCircle.classList.contains('selected-circle-hover'))
                text.innerHTML = 'Головы щучьи с чесноком да свежайшая сёмгушка.';
            else
                text.innerHTML = 'Чего сидишь? Порадуй котэ, <span class="buy buy-btn-2" onclick="buy(event)">купи</span>.';
        }
    });

    thirdProduct.addEventListener('click', (event) => {

        if (!thirdProduct.closest('.product').classList.contains('disabled')) {
            let text = document.getElementsByClassName('product-3-text')[0];

            let targetElement = event.target || event.srcElement;

            targetElement = targetElement.closest('.product');

            let productCircle = targetElement.getElementsByClassName('product-weight')[0];

            if (productCircle.classList.contains('selected-circle-hover'))
                text.innerHTML = 'Филе из цыплят с трюфелями в бульоне.';
            else
                text.innerHTML = 'Чего сидишь? Порадуй котэ, <span class="buy buy-btn-3" onclick="buy(event)">купи</span>.';
        }
    });
});

/**
 * Функция обработки нажатия на слово "Купи" в описании карточки
 * @param event
 */
function buy(event) {
    let targetElement = event.target || event.srcElement;

    targetElement = targetElement.closest('.product');

    if (!targetElement.classList.contains('disabled')) {
        let productTriangle = targetElement.getElementsByClassName('product-triangle')[0];
        let productTop = targetElement.getElementsByClassName('product-top')[0];
        let productBottom = targetElement.getElementsByClassName('product-bottom')[0];
        let productCircle = targetElement.getElementsByClassName('product-weight')[0];

        productTriangle.classList.add('selected-border-angle');
        productTriangle.classList.remove('selected-border-angle-hover');

        productTop.classList.add('selected-border-top');
        productTop.classList.add('selected-border-right');
        productTop.classList.remove('selected-border-top-hover');
        productTop.classList.remove('selected-border-right-hover');

        productBottom.classList.add('selected-border-left');
        productBottom.classList.add('selected-border-right');
        productBottom.classList.add('selected-border-bottom');
        productBottom.classList.remove('selected-border-left-hover');
        productBottom.classList.remove('selected-border-right-hover');
        productBottom.classList.remove('selected-border-bottom-hover');

        productCircle.classList.add('selected-circle');
        productCircle.classList.remove('selected-circle-hover');

        if ((event.target || event.srcElement).classList.contains('buy-btn-1'))
            (event.target || event.srcElement).closest('.product-text').innerHTML = 'Печень утки разварная с артишоками.';

        if ((event.target || event.srcElement).classList.contains('buy-btn-2'))
            (event.target || event.srcElement).closest('.product-text').innerHTML = 'Головы щучьи с чесноком да свежайшая сёмгушка.';

        if ((event.target || event.srcElement).classList.contains('buy-btn-3'))
            (event.target || event.srcElement).closest('.product-text').innerHTML = 'Филе из цыплят с трюфелями в бульоне.';
    }
}